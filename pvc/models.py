from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Scientist(models.Model):
    first_name = models.CharField('First Name', max_length=50)
    last_name = models.CharField('Last Name', max_length=50)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class Plastic(models.Model):
    name = models.CharField('Plastic Name', max_length=500)
    formula = models.CharField('Formula', max_length=250)
    discovery_date = models.DateField('Date of discovery')

    def __str__(self):
        return self.name
