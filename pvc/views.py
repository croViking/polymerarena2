# Create your views here.
from django.http import HttpResponse
from django.http import Http404
from django.template import loader
from django.shortcuts import render

from .models import Plastic


def index(request):
    pvc_polymer_list = Plastic.objects.order_by('-discovery_date')[:5]
    template = loader.get_template('pvc/index.html')
    context = {
        'pvc_polymer_list': pvc_polymer_list,
    }
    return HttpResponse(template.render(context, request))

def detail(request, plastic_id):
    try:
        plastic = Plastic.objects.get(pk=plastic_id)
    except Plastic.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'pvc/detail.html', {'plastic': plastic})
