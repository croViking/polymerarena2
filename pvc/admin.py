from django.contrib import admin

# Register your models here.

from .models import Scientist, Plastic

admin.site.register(Scientist)
admin.site.register(Plastic)